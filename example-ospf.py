import yaml
from getpass import getpass
from jnpr.junos import Device
from jnpr.junos.utils.config import Config
from jnpr.junos.op.ospf import OspfNeighborTable
from jnpr.jsnapy import SnapAdmin
from time import sleep
from pprint import pprint

hosts = ["crpd_ja1","crpd_ja2"]
junos_username = "root" #please modify
junos_password = getpass("Junos OS password: ")
js = SnapAdmin()


def load_ospf_config(host,dev):
    with open("host_vars/{}.yml".format(host)) as f:
        vars = yaml.load(f)

    with Config(dev, mode="exclusive") as conf:
        conf.load(template_path="templates/ospf.j2", template_vars=vars, format='text')
        conf.pdiff()
        if conf.diff() is not None:
             conf.commit()

def get_full_neighbor_count(host):
    count = 0
    with Device(host=host, user=junos_username, passwd=junos_password, port=22) as dev:   
        ospf_table = OspfNeighborTable(dev).get()
        for neighbor in ospf_table:
            if neighbor.ospf_neighbor_state == "Full":
                count += 1
    return count

if __name__ == "__main__":
    #1. Render and load the OSPF configuration on all hosts
    for host in hosts:
        print("Taking pre snapshot on {}..".format(host))
        with Device(host=host, user=junos_username, passwd=junos_password, port=22) as dev:
            #snapPre = js.snap({'tests': ['test_ospf_present.yml']},"pre", dev)
            print("Loading configuration on {}; the diff is:".format(host))
            load_ospf_config(host,dev)

    #2. Check that neighbors get to FULL state
    ok_hosts = set()
    for _ in range(20):
        print("Waiting for OSPF to come up...")
        sleep(5)
        for host in hosts:
            if host not in ok_hosts:
                if get_full_neighbor_count(host) >= 1:
                    ok_hosts.add(host)
        if len(ok_hosts) == len(hosts):
            print("Success!")
            break
    else:
        print("Something went wrong, please check.")
    
    #3. Check current runtime status agains Jsnapy tests.
    for host in hosts:
        with Device(host=host, user=junos_username, passwd=junos_password, port=22) as dev:   
            print("Taking post snapshot on {}..".format(host))
            #snapPost = js.snap({'tests': ['test_ospf_present.yml']},"post", dev)
            #chk = js.check({'tests': ['test_ospf_present.yml']}, "pre", "post", dev)
            snapChk = js.snapcheck({'tests': ['test_ospf_present.yml']}, "post", dev)

            tests_failed = False
            for val in snapChk:
                if val.no_failed != 0:
                    tests_failed = True
            #4. If any test fails, then this configuration should be rolled back
            if tests_failed:
                print("TESTS FAILED, rolling back the configuration")
                with Config(dev, mode="exclusive") as conf:
                    conf.rollback(rb_id=1)
                    print ("Committing the configuration")
                    conf.pdiff()
                    if conf.diff() is not None:
                         conf.commit()
