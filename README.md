# junos_automation
This repository includes some basic examples for using Juniper's PyEZ and Ansible modules

## Requirements
For PyEZ (https://github.com/Juniper/py-junos-eznc)
```
pip install junos-eznc
pip install jsnapy
```
You can also create a virtual environment to isolate this python project by doing:

```
virtualenv --python python3 .
source bin/activate #starts the venv
pip install -r requirements.txt #this will install all required dependencies
```
Once you want to leave the venv:
```
deactivate
```
For Ansible (Juniper roles: https://github.com/Juniper/ansible-junos-stdlib) you must install it as well as the Juniper.junos roles module
(example for Ubuntu)
```
sudo apt-get install ansible
ansible-galaxy install juniper.junos
```

For using the cRPD
- docker
- crpd docker image (Download explanations below)

### Building a container-based testing p2p topology
cRPD is Juniper routing protocol stack decoupled from JUNOS and packaged for Linux container environments.

Deployable on demand as a microservice
Lightweight: 3 seconds startup, ~ 350 MB image size, ~ starting from 100MB RAM
200M RIB, 48K routes/sec FIB download
Can program Linux FIB: IPv4, IPv6, MPLS
NETCONF, OpenConfig, CLI
Automation, Telemetry,  Programmability
Supports Kubernetes, Docker swarm


#### Download
Juniper makes it easy for you to see how effective our containerized routing protocol daemon (cRPD) can be in your own environment. Simply download, install, and start using cRPD.
Start your 90 days free trial today: [crpd_trial](https://www.juniper.net/us/en/dm/crpd-trial/#:~:text=To%20start%20your%20free%20cRPD%20trial%2C%20complete%20the%20following%20steps.&text=You'll%20need%20to%20enter,user%20license%20agreement%20to%20proceed.&text=The%20license%20key%20activates%20your,and%20enables%20advanced%20routing%20features.&text=Follow%20the%20instructions%20to%20install%20your%20license%20key.)

General use cases for cRPD

cRPD can be deployed as BGP Route-Reflector/Route-Server
cRPD might be used as Routing-Daemon on a Whitebox-switch or some custom hardware to build your own router
cRPD might be used for the "Routing on the Host (RotH)" purpose.
cRPD can be used to build testing topologies as explained below

#### Deployment
To deploy the containers
```
cd build_crpd
sudo ./build_p2p.sh
```
To connect to the containers
```
docker exec -it crpd_ja1 sh #console connection
cli #junos cli program

ssh lab@172.17.0.3 #ssh connection
```
To destroy the containers and configuration
```
cd build_crpd
sudo ./destroy_p2p.sh #modify if you want to keep the configuration but destroy the containers
```
#### Complex topologies
More complex topologies can be created as explained in this repository: https://github.com/ARD92/crpd-topology-builder

## OSPF provisioning with PyEZ
Please modify the hardcoded variables as needed in *example_ospf.py*
The ospf.j2 jinja template is stored in the /templates folder
Variables are being loaded from the host_vars folder. Please rename according to the hostnames you are using
Finally, to run the script:
```
python3 example-ospf.py
```

## BGP provisioning with Ansible
According to ansible best practices, credentials for the devices are stored inside a provider in the *group_vars* folder for each device group listed in your *./hosts* file.
Specific host variables are stored in *host_vars* which will be used to render the templates.
To trigger the ansible playbook:
```
ansible-playbook pb-provision-bgp.yml
```
Note: When provisitioning the bgp configurarion on the first time, set the initial boolean to true.

Additional playbook examples are available at: https://github.com/Juniper/juniper_junos_ansible_modules_examples

## Other examples
There are other scripts in this repository. Try them out if you wish
