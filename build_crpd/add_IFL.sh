#!/bin/bash

sudo ip link add link ens8f0 name ens8f0.143 type vlan id 143
sudo ip link set ens8f0.143 netns crpd_ja1
sudo ip -n crpd_ja1 addr add 192.168.60.5/30 dev ens8f0.143
sudo ip -n crpd_ja1 link set ens8f0.143 up

sudo ip link add link ens8f0 name ens8f0.142 type vlan id 142
sudo ip link set ens8f0.142 netns crpd_ja1
sudo ip -n crpd_ja1 addr add 192.168.60.1/30 dev ens8f0.142
sudo ip -n crpd_ja1 link set ens8f0.142 up
