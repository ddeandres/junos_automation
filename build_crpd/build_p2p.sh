#!/bin/bash

create_vol () {
#check if volumes exist. If not, create them.
    if [[ $(docker volume ls | grep -q $1; echo $?) ]];
        then
            echo "docker volume $1 does not exist. Creating..."
            docker volume create $1
            echo "docker volume $1 created"
        else
            echo "docker volume $1 already present. Will be reutilised..."
    fi
}
echo "Building crpd-junos-automation image..."
docker build -t crpd-junos-automation .

echo "Handling required volumes..."
#create_vol crpd_ja1_varlog
#create_vol crpd_ja1_config
#create_vol crpd_ja2_varlog
#create_vol crpd_ja2_config

echo "Launching containers..."
docker run --rm --detach --name crpd_ja1 -h crpd_ja1 --privileged --net=bridge -v $(pwd)/ja1:/config -it crpd-junos-automation
docker run --rm --detach --name crpd_ja2 -h crpd_ja2 --privileged --net=bridge -v $(pwd)/ja2:/config -it crpd-junos-automation

echo "Labling namespaces..."
pid=$(docker inspect crpd_ja1 --format '{{ .State.Pid }}') # get pid of docker-instance
echo "PID of docker crpd_ja1: $pid"

# link the namespace from the VNF/crpd to the :
mkdir -p /var/run/netns
sudo ln -sf /proc/$pid/ns/net /var/run/netns/crpd_ja1
echo "created namespace crpd_ja1"

pid=$(docker inspect crpd_ja2 --format '{{ .State.Pid }}') # get pid of docker-instance
echo "PID of docker crpd_ja2: $pid"

# link the namespace from the VNF/crpd to the :
mkdir -p /var/run/netns
sudo ln -sf /proc/$pid/ns/net /var/run/netns/crpd_ja2
echo "created namespace crpd_ja2"

echo "Connecting crpd_ja1 and crpd_ja2..."
sudo ./connect_ns.sh -s crpd_ja1 -d crpd_ja2 -p 10.0.0.4
