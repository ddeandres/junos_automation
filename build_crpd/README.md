# Instructions
This folder contains the files required to build the automation crpd image as well as deploy the p2p topology used in the examples

## Dockerfile
Includes the image tag used. Please adapt to the tag name that you download or use "crpd:latest"
Also includes the root password for logging to the containers.
One can also log himself by using without this password:
```
docker exec -it crps_ja1 sh
```

## Configuration files
Please create folders which will be mounted to the containers and which will contain the configuration and license files:
```
mkdir ja1/
mkdir ja2/
```

## License
Please paste to each of the ja1/2 folders the license provided by Juniper with name: "license.lic"
Then log to the containers and do:
```
docker exec -it crps_ja1 cli
request system license add  /config/license.lic
```
Once this license is added manually once, subsequent instantiation of the containers using the ja1/2 folders will pick the license automatically

## Other files
Include the automation required to create the containers and attach the veth pairs to the corresponding networking namespace.
Please explore them if you are curious.

