#!/bin/bash
docker stop crpd_ja1 crpd_ja2
#docker volume rm crpd_ja2_config crpd_ja2_varlog
#docker volume rm crpd_ja1_config crpd_ja1_varlog
docker image rm crpd-junos-automation
echo 'deleting ja1 configuration...'
rm ./ja1/juniper.conf.*
echo 'deleting ja2 configuration...'
rm ./ja2/juniper.conf.*
echo 'cleanup COMPLETE!'
