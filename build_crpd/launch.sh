#!/bin/bash

# This script copies any relevant configuration files,
# starts the ssh service, and then starts the rest of cRPD.

#cp /juniper.conf /config/juniper.conf
#mkdir /config/license
#cp /license.lic /config/license.lic
cp /sshd_config /etc/ssh/sshd_config

service ssh start


eval exec /sbin/runit-init 0

#cli request system license add /config/license.txt
